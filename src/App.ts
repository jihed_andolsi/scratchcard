import * as PIXI from "pixi.js";


export class Game extends PIXI.Application {

    constructor() {
        var app = super(640, 360, {backgroundColor: 0x333333, legacy: true});
        document.body.appendChild(app.view);

        // set stage
        this.stage = this.stage;

        // preload the assets
        this.preload();
    }

    public preload(): void {
        PIXI.loader.add("Components/coin.json");

    }
}

window.onload = () => {
    const game = new Game();
};